package fr.esme.esme_map



import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AbsListView
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import java.io.IOException
import java.util.*


class DevicesActivity : AppCompatActivity() {

    //Liste des appareils bluetooth aux alentours
    private val devicesAround = java.util.ArrayList<BluetoothDevice>()
    //Liste des device.name des appareils bluetooth aux alentours pour la List View
    private var devicesName = java.util.ArrayList<String>()
    //List view qui contiendra les devicesName
    private lateinit var mListView : ListView
    private lateinit var bluetoothAdapter: BluetoothAdapter
    private lateinit var selectedDevice : BluetoothDevice
    //Création d'un UUID pour tous les appareils
    private val myDeviceUUID = UUID.fromString("123e4567-e89b-12d3-a456-426614174000")

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        Log.d(MainActivity::class.qualifiedName, "onCreate")
        setContentView(R.layout.activity_devices)

        //Récupération de l'adaptater
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
        }

        //Vérification de l'activation du bluetooth
        if (!bluetoothAdapter?.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            val REQUEST_ENABLE_BT = 1
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }

        //Vérification de la visibilité de l'appareil (ne dure que 120 secondes)
        if(!bluetoothAdapter?.isDiscovering){
            val discoverableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            val REQUEST_DISCOVERABLE_BT = 1
            startActivityForResult(discoverableBtIntent, REQUEST_DISCOVERABLE_BT)
        }

        /****************** Principe ******************/
        /* Chaque appareil peut faire trois choses : **/
        /*  - recevoir des demandes de connexion ******/
        /*  - envoyer des demandes de connexion *******/
        /*  - voir les appareils aux alentours ********/
        /**********************************************/

        // Lancement de l'inner class AcceptThread()
        val serveur = AcceptThread()
        serveur.start()

        // lancement de la recherche d'appareils aux alentours
        bluetoothAdapter?.startDiscovery()

        // Register for broadcasts when a device is discovered.
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        registerReceiver(receiver, filter)


        // Affichage de la liste des appareils trouvés
        mListView = findViewById<ListView>(R.id.DevicesList)!!

        // Click listener afin de récupérer le nom de la liste sur lequel l'user à cliqué
        mListView.setOnItemClickListener { parent, view, position, id ->
            val element = mListView.getItemAtPosition(position)
            Log.d("TAG", "Tu as cliqué sur " + element)
            // Une fois l'élement récupéré, on récupère le device associé
            devicesAround.forEach {
                if (it.name == element) {
                    selectedDevice = it
                }
            }
            if(selectedDevice != null){
                // Une fois le device associé récupéré, on demande de faire le pairing en tant que client dans l'inner class ConnectThread avec le device associé
                Log.d("TAG", "j'ai trouvé " + selectedDevice.name + " avec l'adresse MAC : " + selectedDevice.address)
                val client = ConnectThread(selectedDevice)
                client.start()
            }
        }
    }



    // Create a BroadcastReceiver for ACTION_FOUND.
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action: String? = intent.action
            when(action) {
                BluetoothDevice.ACTION_FOUND -> {
                    // Discovery has found a device. Get the BluetoothDevice
                    // object and its info from the Intent.
                    val device: BluetoothDevice? =
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    if (device != null) {
                        // On ajoute l'appareil trouvé non nul à la liste des appareils autour
                        if (!devicesAround.contains(device)) {
                            devicesAround.add(device)
                            Log.d("TAG", "J'ai découvert " + device.name)
                            showDevices()
                        }
                    }
                }
            }
        }
    }

    // Fonction qui ajoute tous les noms de la liste devicesAround dans la liste devicesName pour les ajouter à la ListView
    private fun showDevices(){
        devicesAround.forEach(){
            if(it.name != null){
                if(!devicesName.contains(it.name)) {
                    devicesName.add(it.name)
                }
            }
        }
        // On utilise un adapteur pour lier la nouvelle liste et la ListView
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, devicesName)
        mListView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        // Don't forget to unregister the ACTION_FOUND receiver.
        unregisterReceiver(receiver)
    }

    // Inner class qui permet de lancer un thread sur les demandes de connexion que l'appareil peut recevoir
    private inner class AcceptThread : Thread() {

        private val mmServerSocket: BluetoothServerSocket? by lazy(LazyThreadSafetyMode.NONE) {
            bluetoothAdapter?.listenUsingInsecureRfcommWithServiceRecord(bluetoothAdapter.name, myDeviceUUID)
        }

        override fun run() {
            // Keep listening until exception occurs or a socket is returned.
            var shouldLoop = true
            while (shouldLoop) {
                Log.e("TAG", "Entré dans la loop")
                val socket: BluetoothSocket? = try {
                    Log.e("TAG", "Tentative d'acceptation")
                    mmServerSocket?.accept()
                } catch (e: IOException) {
                    Log.e("TAG", "Socket's accept() method failed", e)
                    shouldLoop = false
                    null
                }
                socket?.also {
                    Log.e("TAG", "Socket connection completed")
                    //manageMyConnectedSocket(it)
                    mmServerSocket?.close()
                    shouldLoop = false
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        fun cancel() {
            try {
                mmServerSocket?.close()
            } catch (e: IOException) {
                Log.e("", "Could not close the connect socket", e)
            }
        }
    }

    // Inner class qui permet de lancer un thread sur les demandes de connexion que l'appareil peut envoyer
    private inner class ConnectThread(device: BluetoothDevice) : Thread() {

        private val mmSocket: BluetoothSocket? by lazy(LazyThreadSafetyMode.NONE) {
            device.createRfcommSocketToServiceRecord(myDeviceUUID)
        }

        public override fun run() {
            // Cancel discovery because it otherwise slows down the connection.
            bluetoothAdapter?.cancelDiscovery()

            mmSocket?.use { socket ->
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                Log.d("TAG", "Je vais me connecter à " + selectedDevice.name)
                socket.connect()

                if(socket.isConnected){
                    Log.d("TAG", "Je suis connecté à " + selectedDevice.name)
                    returnToMainActivity()
                } else {
                    Log.d("TAG", "Je ne suis pas connecté à " + selectedDevice.name)
                }

                // The connection attempt succeeded. Perform work associated with
                // the connection in a separate thread.
                //manageMyConnectedSocket(socket)
            }
        }

        // Closes the client socket and causes the thread to finish.
        fun cancel() {
            try {
                mmSocket?.close()
            } catch (e: IOException) {
                Log.e("TAG", "Could not close the client socket", e)
            }
        }
    }

    private fun returnToMainActivity(){
        val intent = Intent(this, MainActivity::class.java).apply {

        }
        startActivity(intent)
    }


}